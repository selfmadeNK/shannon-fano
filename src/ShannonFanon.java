
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Papa Momar
 */
public class ShannonFanon {
	private final HashMap<String, String> SymbolesCodes;
	private final HashMap<String, Double> probabilitesDesSymoles;
        private final  List<String> ListeSymboles;
	private double entropie;
        private double longueurMoyenneParSymbole;
        private double efficacite;
        
        //Constructeur
        public ShannonFanon(HashMap<String, Double> probabilitesDesSymoles) {
        this.probabilitesDesSymoles = ordonnerListeProbabilites(probabilitesDesSymoles);
        this.ListeSymboles= new ArrayList<>(this.probabilitesDesSymoles.keySet());
        this.SymbolesCodes=new HashMap<>();
    }
      
   
private HashMap<String,Double> ordonnerListeProbabilites(HashMap<String,Double> liste){
    HashMap<String, Double> listeResultante=new HashMap<String,Double>();
    SortedSet<Map.Entry<String,Double>> listeDePairesOrdonnees= new TreeSet<Map.Entry<String,Double>>(new Comparator<Map.Entry<String,Double>>(){
        @Override
        public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
           int resultat=o1.getValue().compareTo(o1.getValue());
           return resultat;
        }
    }.reversed());
   Set monSet=liste.entrySet();
    for (Object entry : monSet) {
        listeDePairesOrdonnees.add((Map.Entry<String,Double>)entry);     
    }
    listeDePairesOrdonnees.addAll(monSet);
     System.out.println(listeDePairesOrdonnees.size());
    for(Map.Entry<String, Double> paire : listeDePairesOrdonnees)
    {
        listeResultante.put(paire.getKey(),paire.getValue());
    }
   
    return listeResultante;
}        
private int indiceDeSeparation(List<String> listeDeSymboles,HashMap<String, Double> probabilitesDesSymoles){
    int indice=0;
    Double moitie=0.000, somme=0.000;

    for (String symbole : listeDeSymboles) {
        moitie+=probabilitesDesSymoles.get(symbole);
    }
    moitie=moitie/2;

 for (String symbole : listeDeSymboles){
        indice++;
        somme+=probabilitesDesSymoles.get(symbole);
        if(somme>moitie && (somme-moitie)>(moitie-(somme-probabilitesDesSymoles.get(symbole)))){
            indice--;
            break;
        }else if(somme>moitie){
            break;
        }
    }    
    return indice;
}


private void ajouterLesBits(List<String> listeDesSymboles, boolean up){
    String bit = "";
		if (!SymbolesCodes.isEmpty()) {
                        if(up){
                            bit="0";
                        }else bit="1";
		}

            for (String c : listeDesSymboles) {
                String s="";
                if(SymbolesCodes.get(c) != null){
                    s=SymbolesCodes.get(c);
                }
                SymbolesCodes.put(c, s + bit);
            }

		if (listeDesSymboles.size() >= 2) {
                    int separateur = this.indiceDeSeparation(listeDesSymboles, this.probabilitesDesSymoles);
                    List<String> upList = listeDesSymboles.subList(0, separateur);
                    ajouterLesBits(upList, true);
                    List<String> downList = listeDesSymboles.subList(separateur, listeDesSymboles.size());
                    ajouterLesBits(downList, false);
                }
}

 public void coder(){
   this.ajouterLesBits(this.ListeSymboles, true);
}
 
 private double getEntropie() {

    for (Double probabilite : this.probabilitesDesSymoles.values()) {
    
        this.entropie += probabilite* (Math.log(1.0 / probabilite) / Math.log(2));
    }
    return entropie;
}

 private double getLongueurMoyenneParSymbole() {
     
    for(Map.Entry<String,String> paireSymboleCode : SymbolesCodes.entrySet()) {
       
        this.longueurMoyenneParSymbole+=paireSymboleCode.getValue().length()*this.probabilitesDesSymoles.get(paireSymboleCode.getKey());    
    }
        return longueurMoyenneParSymbole;
    }

    private double getEfficacite() {
        this.efficacite=(this.getEntropie()/this.getLongueurMoyenneParSymbole())*100;
        return efficacite;
    }
 
 
  @Override
    public String toString() {
        String s="";
        for (Map.Entry<String, String> paire : SymbolesCodes.entrySet()) {
            
            s+=paire.getKey()+":   "+paire.getValue()+"\n";
        }
        s+="\n\nH(x) = "+this.getEntropie()+"\n";
        s+="L = "+this.getLongueurMoyenneParSymbole()+"\n";
        s+="Efficacite = "+this.getEfficacite()+ "%";
        return s;
    }

}
