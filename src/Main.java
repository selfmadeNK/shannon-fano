
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Papa Momar
 */
public class Main {
     public static void main(String[] args) {
         HashMap<String, Double> probability = new HashMap<String, Double>();

		probability.put("x1", 0.38461538);
		probability.put("X2", 0.17948718);
		probability.put("X3", 0.15384615);
		probability.put("X4", 0.15384615);
		probability.put("X5", 0.12820513);
		//probability.put("x6", 0.05);
          ShannonFanon codage=new ShannonFanon(probability);
          codage.coder();
          JOptionPane.showMessageDialog(null, codage.toString(), "Résultats du codage", JOptionPane.INFORMATION_MESSAGE);
          
    }
}
